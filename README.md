# Тестовое задание

## Задание:

1. Спроектировать структуру БД (PostgreSQL) по заданному описанию API

    - Описать, из каких таблиц должна состоять БД
    
    - Учесть правила нормализации данных
    
    - Указать первичные ключи, уникальные значения, ограничения (constraint)
    
    - Описать необходимые индексы
    
    - Написать запросы на создание таблиц

2. Разработать общую структуру REST API на python (Django или Flask по желанию) – описать классы, методы, свойства, без реализации.


### API сервиса

Сервис предоставляет разработчикам RESTful API для интеграции.
Основа JSON (кодировка UTF 8 ).
Сервис доступен по ссылке ​ https://bill.tech.ru/api/v 1. 0 /
Для работы с API требуется uuid.

### uuid - обязательный параметр для всех методов

(Universally Unique Identifier) - Последовательность из 32 - х шестнадцатеричных цифр ( 0 — 9 , A—F), предназначенная
для авторизации в данном сервисе. Выдается администратором системы.
```
Пример uuid    c9ea3637b1a0486abcf4ce46fe6851d2c9ea3637b1a0486abcf4ce46fe6851d2
```
### Формат даты согласно ISO 8601 : 2004

YYYYMMDDThhmmss

```
Пример даты    20130225T182510 - 25 февраля 2013 года 18 часов 25 минут 10 секунд
```
### Уточнение формат параметров

**sum** - передается в минимальных значениях валюты счета (Копейки, Центы и тд);

### Передача параметров методу

**Тип обращения GET** - ​получение (чтение) представления ресурса. В случае “удачного” (или не содержащего
ошибок) адреса, GET возвращается представление ресурса в формате JSON в сочетании с кодом состояния HTTP 200
(OK). В случае наличия ошибок обычно возвращается код 404 (NOT FOUND) или 400 (BAD REQUEST).

**Тип обращения PUT** - ​обновление ресурса. Тело запроса при отправке PUT-запроса к существующему ресурсу URI
должно содержать обновленные данные оригинального ресурса (полностью, или только обновляемую часть).
При успешном обновлении посредством выполнения PUT запроса возвращается код 200 (или 204 если не был
передан какой либо контент в теле ответа). Если PUT используется для создания экземпляра - обычно возвращают HTTP
код 201 при успешном создании. Возвращать данные в ответ на запрос не обязательно. Также не обязательно
возвращать ссылку на экземпляр ресурса посредством заголовка `Location` по причине того, что клиент и так обладает
идентификатором экземпляра ресурса.

**Тип обращения POST** - ​создание новых ресурсов. На практике он используется для создания вложенных
ресурсов. Другими словами, при создании нового ресурса, POST запрос отправляется к родительскому ресурсу и, таким
образом, сервис берет на себя ответственность на установление связи создаваемого ресурса с родительским ресурсом,
назначение новому ресурсу ID и т.п.
При успешном создании ресурса возвращается HTTP код 201 , а также в заголовке `Location` передается адрес
созданного ресурса.

**Тип обращения DELETE** ​ - ​удаление​ ресурса, идентифицированного конкретным URI (ID).
При успешном удалении возвращается 200 (OK) код HTTP, совместно с телом ответа, содержащим данные
удаленного ресурса (отрицательно сказывается на экономии трафика) или завернутые ответы (Смотрите "Возвращаемые
данные"). Также возможно использование HTTP кода 204 (NO CONTENT) без тела ответа.

Параметры передаются методам в формате JSON (кодировка UTF 8 ). Отсутствие любого обязательного параметра
вызовет ошибку. В случае отсутствия необязательного параметра будет использовано значение этого параметра
заданное по умолчанию (в документе указано в скобках).

### Возвращаемые значения

Методы возвращают результат выполнения в формате JSON (кодировка UTF 8 ).

### Обязательные возвращаемые значения


При использовании любого метода ответ сервера вместе с различными данными будет передавать:

**operation_id** - идентификатор проведенной операции
**operation_time** - время затраченное на проведение операции

### Метод - Создать аккаунт (запись Лицевого Счета)
Метод создает запись ЛС/аккаунт

#### Параметр метода
**email** ​ - ;

**mobN** ​ - ;

**agent** ​ - Индекс Компании с которой заключен договор;
​
**contract** - Номер договора с агентом;

**status** - Булево (TRUE);

**type** - Проверенный, не проверенный и тд;

**tag** - Любые текстовые теги. Например, получен скан паспорта, неплательщик, вип ...;

**region** ​ - Регион (страна) для которого ЛС создан

Название региона | Код региона 
--- | --- 
Великобритания | GBP 
Канада | CAN 
Евросоюз | EUR 
Россия | RUS 
США | USA 

**currency** ​ - Валюта ЛС. По умолчанию устанавливается в соответствии с регионом
Есть стандартные справочники по валютам от ЦБ РФ:

[http://www.cbr.ru/mcirabis/KV/](http://www.cbr.ru/mcirabis/KV/)

[http://www.cbr.ru/development/DWS/](http://www.cbr.ru/development/DWS/)

Название валюты | Знак валюты | Код валюты
--- | --- | ---
Британский фунт | GBP | 826
Канадский доллар | CAD | 124
Единая европейская валюта | EUR | 978
Российский рубль | RUB | 643
Американский доллар | USD | 840

**trust** - Размер кредита доверия Значение по умолчанию = “NULL”. Установленное значение позволяет списывать
деньги с ЛС уводя его в отрицательное значение на указанное значение ​ **Кредит доверия** - сумма на которую ЛС
Договора может уйти в минус (овердрафт). Эта сумма не ограничена по времени;

**limitTransaction** - Размер лимита списания денежных средств за одну транзакцию Значение по умолчанию =
“NULL”. Установленное значение определяет лимит списания денег за каждую транзакцию. Размер лимита списания
денежных средств за одну транзакцию;Кто конкретно проводил операции с системой.
Определять пользователя системы по UUID.

**limitPerDay** - Значение по умолчанию = “NULL”. Установленное значение определяет лимит списания денег за
сутки.

---

Тип обращения: POST

Адрес метода: /add_account/

### Параметры передаваемые методу:

```
Обязательные
● uuid - Строка;
● email - Строка (уникальное значение);
● mobN - Строка (уникальное значение);
● agent - Строка;
```
```
Не обязательные (option)
● contract - Строка (NULL);
● status - Булево (TRUE);
● type - Строка (по умолчанию ???);
● tag - Строка;
● region - Строка (из uuid);
● currency - Строка (из uuid);
● trust - Число (0);
● limitTransaction - Число (0);
● limitPerDay - Число (0).
```
#### Пример запроса:

JSON:

```
{ "uuid":"2jeOtVSVhl0P32G5g1EvMwTzaR7GnDdE",
"email":"test@domain.com",
"mobN":79265952625,
"agent":"THR",
"contract":"TH1234" }
```
#### Возвращаемые значения в случае успешного выполнения:

```
login - Число;
pass - Строка;
operationId - Число;
operationTime - Строка;
http значение - 201.
```